# problem 1

countries = {
  "Asia":["China", "Mongolia", "India"],
  "South America": ["Brazil", "Argentina", "Chile"],
  "North America": ["United States", "Canada", "Mexico"], "Antarctica": [],
  "Africa": ["South Africa", "Algeria", "Kenya", "Ethiopia", "Egypt"],
  "Europe": ["France", "Germany", "England", "Spain", "Greece", "Italy"],
  "Australia": ["New Zealand", "Fiji", "Australia"]
  }

print(countries["Asia"])
print(countries["Australia"][2])
print(sorted(countries))

# Problem 2

info = { "first_name": "W.L.",
  "last_name": "McCrea",
  "company": "Voltage Ad",
  "company_location": "Louisville, Kentucky",
  "years_experience": 4,
  "role": "graphic designer"
}

print(f'the candidate\'s name is {info["first_name"]} {info["last_name"]}. Shehas {info["years_experience"]} of experience as a {info["role"]} at  {info["company"]} in {info["company_location"]}')

# Problem 3

letter =["s", "a", "j", "q", "l", "m", "v", "l", "t", "h", "b", "w",
"r", "g", "n", "c", "y", "i", "z", "a", "l", "t", "x", "e",
"k","o", "r", "u", "p", "l", "n", "c", "d", "q", "l", "w"]

# Create a for loop that interates throught all the chars in letters.
# Create a new dictionary to hold the count of each letter.
# In this loop create a key for each letter and add the letter count to it's proper key.

Dict = {}

